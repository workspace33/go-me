package main

import (
	"fmt"
	"sync"
	"time"
)

var wg sync.WaitGroup

func ggg(cache chan int) {
	defer wg.Done()
	cache <- 1
	close(cache)

	time.Sleep(5 * time.Second)
	cache <- 4
}

func main() {

	cache := make(chan int)
	wg.Add(1)
	go ggg(cache)
	d, ok := <-cache
	fmt.Println(d, ok)

	for data := range cache {
		fmt.Println(data)
	}

	d, ok = <-cache
	fmt.Println(d, ok)

	wg.Wait()
}
