package main

import (
	"fmt"
)

func main() {
	cache := make(chan int, 10)
	cache <- 1
	cache <- 2
	cache <- 3

	close(cache)

	d, ok := <-cache
	fmt.Println(d, ok)

	for data := range cache {
		fmt.Println(data)
	}

	d, ok = <-cache
	fmt.Println(d, ok)

	cache <- 4

}
