package main

import (
	"fmt"
	"sync"
)

func printOddNumber(max int, lk1 chan struct{}, lk2 chan struct{}, wg *sync.WaitGroup) {
	defer wg.Done()
	for i := 1; i < max; i += 2 {
		if _, ok := <-lk1; ok {
			fmt.Println(i)
			//打印完最后一个数字，就退出
			if i < max-1 {
				lk2 <- struct{}{}
			}
		}
	}
	close(lk2)
}

func printEvenNumber(max int, lk1 chan struct{}, lk2 chan struct{}, wg *sync.WaitGroup) {
	defer wg.Done()

	for i := 0; i < max; i += 2 {
		if _, ok := <-lk2; ok {
			fmt.Println(i)
			lk1 <- struct{}{}
		}
	}
	close(lk1)
}

func main() {
	lk1 := make(chan struct{})
	lk2 := make(chan struct{})
	wg := &sync.WaitGroup{}
	wg.Add(1)
	go printEvenNumber(100, lk1, lk2, wg)
	wg.Add(1)
	go printOddNumber(100, lk1, lk2, wg)
	lk2 <- struct{}{}
	wg.Wait()

}
