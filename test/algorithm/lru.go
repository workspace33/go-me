package main

import (
	"errors"
	"fmt"
)

type Node struct {
	Val  int
	Key  string
	Pre  *Node
	Next *Node
}

type LRU struct {
	Hs       map[string]*Node
	Head     *Node
	Tail     *Node
	Capacity int
	Len      int
}

func New(cap int) *LRU {
	h := &Node{}
	t := &Node{}
	h.Pre = nil
	h.Next = t
	t.Pre = h
	t.Next = nil
	return &LRU{
		Hs:       make(map[string]*Node),
		Head:     h,
		Tail:     t,
		Capacity: cap,
		Len:      0,
	}
}

func NodeDelete(node *Node) {
	node.Next.Pre = node.Pre
	node.Pre.Next = node.Next
}

func NodeAdd(tail *Node, v int, k string) {
	node := &Node{Val: v, Key: k}
	tail.Pre.Next = node
	node.Pre = tail.Pre
	tail.Pre = node
	node.Next = tail
}

func (l *LRU) Get(key string) (int, error) {
	node, ok := l.Hs[key]
	if ok {
		NodeDelete(node)
		NodeAdd(l.Tail, node.Val, node.Key)
		return node.Val, nil
	}
	return 0, errors.New("not found")
}

func (l *LRU) Put(key string, v int) error {
	node, ok := l.Hs[key]
	if ok {
		NodeDelete(node)
		NodeAdd(l.Tail, node.Val, node.Key)
		return nil
	}
	if l.Capacity == l.Len {
		delete(l.Hs, l.Head.Next.Key)
		NodeDelete(l.Head.Next)
		fmt.Println(l.Hs, l.Head.Next)

		l.Len--
	}
	l.Len++
	NodeAdd(l.Tail, v, key)
	l.Hs[key] = l.Tail.Pre

	return nil
}

func print(lru *LRU) {
	for node := lru.Head.Next; node != lru.Tail; node = node.Next {
		fmt.Printf("%s %d \n", node.Key, node.Val)
	}
}

func main() {
	lru := New(3)
	lru.Put("1", 1)
	lru.Put("2", 2)
	lru.Put("3", 3)
	lru.Put("4", 4)

	fmt.Println(lru.Hs)
	print(lru)
	lru.Get("2")
	print(lru)

}
