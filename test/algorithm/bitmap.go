package main

import (
	"errors"
	"fmt"
	"os"
)

const Base = 32

type BitMap struct {
	words  []int
	sizes  int
	values int
}

func New(values int) (*BitMap, error) {
	if values < 0 {
		return nil, errors.New("valuse error")
	}
	sz := getIndex(values-1) + 1
	return &BitMap{
		sizes:  sz,
		values: values,
		words:  make([]int, sz),
	}, nil
}

func getIndex(v int) int {
	return v / Base
}

func getMvBits(v int) int {
	bt := v % Base
	return bt
}

func (bm *BitMap) setBit(v int) error {
	if v < 0 || v > bm.values-1 {
		return errors.New("value error")
	}
	index := getIndex(v)
	mvBits := getMvBits(v)
	fmt.Println(index, mvBits)
	bm.words[index] |= 1 << mvBits

	return nil
}

func (bm *BitMap) getBit(v int) (bool, error) {
	if v < 0 || v > bm.values-1 {
		return false, errors.New("value error")
	}
	index := getIndex(v)
	mvBits := getMvBits(v)
	fmt.Println(index, mvBits)
	return bm.words[index]&(1<<mvBits) != 0, nil
}

func main() {

	bm, err := New(128)
	if err != nil {
		os.Exit(-1)
	}
	bm.setBit(3)
	bm.setBit(64)
	bm.setBit(127)
	bm.setBit(0)

	fmt.Println(bm.words)

	res, _ := bm.getBit(3)
	fmt.Println(res)
	res, _ = bm.getBit(64)
	fmt.Println(res)
	res, _ = bm.getBit(127)
	fmt.Println(res)

	res, _ = bm.getBit(0)
	fmt.Println(res)

}
