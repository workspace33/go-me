package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"net/http"
	"strings"
	"time"
)

var (
	HistVec = prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Name:    "http_duration_seconds_bucket",
		Help:    "server requests duration(sec).",
		Buckets: []float64{0.005, 0.01, 0.025, 0.05, 0.1, 0.250, 0.5, 1, 1.005, 1.01, 1.025, 1.05, 1.1, 1.250, 1.5, 2},
	}, []string{"client"})

	CntVec = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "requests_total",
		Help: "The total number of processed requests",
	}, []string{"client"})
)

func MetricsOp(startTime time.Time, r *http.Request) {

	clentIp := prometheus.Labels{"client": strings.Split(r.RemoteAddr, ":")[0]}
	CntVec.With(clentIp).Inc()
	HistVec.With(clentIp).Observe(time.Since(startTime).Seconds())
}

func MetricsRegister() {
	prometheus.MustRegister(HistVec, CntVec)
}
