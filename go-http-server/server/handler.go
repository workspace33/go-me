package server

import (
	"encoding/json"
	"fmt"
	"github.com/go-http-server/metrics"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/shopspring/decimal"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"math/rand"
	"net/http"
	"time"
)

type Handler struct {
	Router *mux.Router
	Log    *log.Logger
}

type UsPs struct {
	UserName string `"json:username"`
	Password string `"json:password"`
}

func Register(H *Handler) {
	H.Router.HandleFunc("/go/readyz", H.IsReadyz).Methods("GET")
	H.Router.HandleFunc("/go/livez", H.IsLivez).Methods("GET")
	H.Router.HandleFunc("/go/hello", H.Hello).Methods("GET")
	H.Router.HandleFunc("/go/login", H.Login).Methods("POST")

	H.Router.Handle("/metrics", promhttp.Handler()).Methods("GET")

}

func (h *Handler) IsReadyz(w http.ResponseWriter, r *http.Request) {
	h.Log.Debug("readyz")
	w.Write([]byte("ok!"))
}

func (h *Handler) IsLivez(w http.ResponseWriter, r *http.Request) {
	h.Log.Debug("livez")
	w.Write([]byte("ok!"))
}

func (h *Handler) Hello(w http.ResponseWriter, r *http.Request) {
	h.Log.Debug("hello")
	startTime := time.Now()

	rand.Seed(time.Now().UnixNano())
	delayTimes := rand.Float64() * 2
	v1, _ := decimal.NewFromFloat(delayTimes).Round(3).Float64()

	fmt.Println(v1)
	time.Sleep(time.Second * time.Duration(v1))

	w.Write([]byte("Hello World"))

	metrics.MetricsOp(startTime, r)
}

func (h *Handler) Login(w http.ResponseWriter, r *http.Request) {
	h.Log.Debug("login")
	username, err := ioutil.ReadFile("/etc/userpass/username")
	if err != nil {
		w.Write([]byte("Login fail"))
		w.WriteHeader(http.StatusInternalServerError)
		h.Log.Errorf("login fail, %v", err)
		return
	}
	password, err := ioutil.ReadFile("/etc/userpass/password")
	if err != nil {
		w.Write([]byte("Login fail"))
		w.WriteHeader(http.StatusInternalServerError)
		h.Log.Errorf("login fail, %v", err)
		return
	}
	up := UsPs{}
	decoder := json.NewDecoder(r.Body)
	decoder.UseNumber()
	err = decoder.Decode(&up)
	if err != nil {
		w.Write([]byte("Login fail"))
		w.WriteHeader(http.StatusInternalServerError)
		h.Log.Errorf("login fail, %v", err)
		return
	}

	if up.UserName != string(username) {
		w.Write([]byte("Login fail, username not exist"))
		w.WriteHeader(http.StatusNotFound)
		h.Log.Errorf("user %s login fail, %s", username, "username not exist")
		return
	}

	if up.Password != string(password) {
		w.Write([]byte("Login fail, password error"))
		w.WriteHeader(http.StatusNotFound)
		h.Log.Errorf("user %s login fail, %s", username, "password error")
		return
	}
	w.Write([]byte("Login success"))

	h.Log.Infof("user:%s loging success", username)
}
