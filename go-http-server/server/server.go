package server

import (
	"context"
	log "github.com/sirupsen/logrus"
	"golang.org/x/net/http2"
	"golang.org/x/net/http2/h2c"
	"net/http"
	"time"
)

type Server struct {
	*http.Server
	log *log.Logger
}

// New http server
func New(log *log.Logger, handler http.Handler, addr string, timeout time.Duration, idleTimeout time.Duration) *Server {
	srv := &Server{
		Server: &http.Server{
			Addr: addr,
			Handler: h2c.NewHandler(handler, &http2.Server{
				IdleTimeout: idleTimeout,
			}),
			//Handler: handler,
			ReadTimeout:       timeout,
			ReadHeaderTimeout: timeout,
			WriteTimeout:      timeout,
			IdleTimeout:       idleTimeout,
		},
		log: log,
	}

	return srv
}

func (s *Server) Start(ctx context.Context) error {
	s.log.Infof("start server in %s", s.Addr)
	return s.ListenAndServe()
}

func (s *Server) Stop(ctx context.Context) error {
	s.log.Infof("stoped server in %s", s.Addr)
	return s.Shutdown(ctx)
}
