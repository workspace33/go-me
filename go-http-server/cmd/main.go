package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"github.com/go-http-server/metrics"
	"github.com/go-http-server/server"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"golang.org/x/sync/errgroup"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

type CFG struct {
	Timeout     time.Duration `json:"timeout"`
	IdleTimeout time.Duration `json:"idleTimeout"`
	Addr        string        `json:"addr"`
	Level       string        `json:"level"`
}

var configfile string
var DefaultCfg = CFG{Level: "Debug"}

func init() {
	flag.DurationVar(&DefaultCfg.Timeout, "timeout", time.Second*15, "server timeout, eg: 15s")
	flag.DurationVar(&DefaultCfg.IdleTimeout, "idleTimeout", time.Second*300, "server idleTimeout, eg: 300s")
	flag.StringVar(&DefaultCfg.Addr, "address", ":8080", "server listen address")
	flag.StringVar(&configfile, "configfile", "/etc/httpserver/config.yaml", "config file path")

	metrics.MetricsRegister()
}

func ConfigParse(filepath string) {
	data, err := ioutil.ReadFile(filepath)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(string(data))
	err = yaml.Unmarshal(data, &DefaultCfg)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(DefaultCfg)
}

func main() {
	flag.Parse()
	ConfigParse(configfile)

	logger := log.New()
	logLevelInt, err := log.ParseLevel(DefaultCfg.Level)
	if err != nil {
		logger.Fatal(err)
	}
	logger.SetLevel(logLevelInt)
	logger.Infof("timeout:%v, idleTimeout:%v, address:%s", DefaultCfg.Timeout, DefaultCfg.IdleTimeout, DefaultCfg.Addr)

	ctx, cancel := context.WithCancel(context.Background())
	//error group
	eg, ctx := errgroup.WithContext(ctx)
	wg := sync.WaitGroup{}

	//new server
	h := &server.Handler{Log: logger, Router: mux.NewRouter()}
	server.Register(h)
	srv := server.New(logger, h.Router, DefaultCfg.Addr, DefaultCfg.Timeout, DefaultCfg.IdleTimeout)
	eg.Go(func() error {
		<-ctx.Done() // wait for stop signal
		sctx, scancel := context.WithTimeout(context.Background(), 5)
		defer scancel()
		return srv.Stop(sctx)
	})
	wg.Add(1)
	eg.Go(func() error {
		wg.Done()
		return srv.Start(ctx)
	})
	wg.Wait()

	//signal handle
	signal_c := make(chan os.Signal)
	signal.Notify(signal_c, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
	eg.Go(func() error {
		for {
			select {
			case <-ctx.Done():
				logger.Info("server stop")
				return ctx.Err()
			case <-signal_c:
				logger.Info("server stop")
				cancel()
				return nil
			}
		}
	})
	log.Infof("started")
	if err := eg.Wait(); err != nil && errors.Is(err, context.Canceled) {
		log.Fatal(err)
	}
	log.Infof("exited")
}
