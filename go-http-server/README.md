# httpserver

# 要求
- 优雅启动
- 优雅终止
- 资源需求和 QoS 保证
- 探活
- 日常运维需求，日志等级
- 配置和代码分离
- 支持prometheus采集服务请求处理指标

# 部署
- cd deploy
- kubectl apply -f .


# 服务验证
curl -H "Content-Type:application/json" -X POST --data '{"username": "admin", "password":"123456"}' http://127.0.0.1:31961/go/login
